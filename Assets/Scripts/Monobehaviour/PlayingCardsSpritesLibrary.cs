﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(fileName = "PlayingCardsLibrary", menuName = "Game Data/Card", order = 0)]
public class PlayingCardsSpritesLibrary : ScriptableObject
{
    [System.Serializable]
    public struct CardSpriteList
    {
        public Card.Suit suit;
        public List<Sprite> cardsInOrder;
    }

    public List<CardSpriteList> cardSpriteLists;

    public Sprite GetCardSprite(Card card)
    {
        return cardSpriteLists.First(g => g.suit == card.suit).cardsInOrder[(int)card.rank - 2];
    }
}
