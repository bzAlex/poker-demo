﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Game : MonoBehaviour
{
    const int playerCount = 2;
    const int handCount = 2;
    const int communityCardCount = 5;

    Queue<Card> deck;
    List<Card> communityCards = new List<Card>();
    List<Player> players = new List<Player>();
    HandEvaluator handEvaluator = new HandEvaluator();

    [SerializeField] BoardUI boardUI;
    [SerializeField] CardDistributionController controller;
    [SerializeField] SfxController sfxController;

    void Awake()
    {
        // Create player entities
        for (int i = 0; i < playerCount; ++i)
            players.Add(new Player(i, "Player" + (i + 1)));

        boardUI.PlayButton.onClick.AddListener(Play);
        boardUI.SetPlayerNames(players[0].name, players[1].name);
    }

    void Update()
    {
        // Debug
        if (Input.GetKeyDown(KeyCode.Space))
            Play();
    }

    public void Play()
    {
        // Reset Player hands
        foreach (var p in players)
            p.cardsInHand.Clear();

        // Reset deck
        deck = DeckCreator.CreateDeck();

        // Get 5 community cards
        communityCards = deck.DequeueChunk(communityCardCount).ToList();

        // Give 2 cards per player
        foreach (var p in players)
            p.cardsInHand = deck.DequeueChunk(handCount).ToList();

        // Evaluate each players hand
        var sevenHandCardA = communityCards.Concat(players[0].GetPlayerSet()).ToArray();
        var sevenHandCardB = communityCards.Concat(players[1].GetPlayerSet()).ToArray();

        var result = handEvaluator.CompareHands(sevenHandCardA, sevenHandCardB, out players[0].handResult, out players[1].handResult);

        var winner = "";
        if (result == 0)
            winner = "Draw!";
        else if (result == 1)
            winner = players[0].name;
        else
            winner = players[1].name;

        // Controller methods for visualizing game
        controller.RevealCommunityCards(communityCards.ToArray());
        controller.DistributePlayerCards(players[0].cardsInHand.ToArray(), players[1].cardsInHand.ToArray());

        // Update UI
        boardUI.DetermineWinner(result);

        boardUI.ShowResultingCard(players[0], players[1]);
        DebugLogs(winner);
        sfxController.WinSound();
    }

    void DebugLogs(string winner)
    {
        // List cards for each
        for (int i = 0; i < playerCount; ++i)
        {
            var s = "";
            foreach (var r in players[i].handResult.resultCards)
            {
                s = String.Concat(s, (r.ToString() + " "));
            }
            Debug.Log(players[i].name + " = " + players[i].handResult.kind + "=>" + s);
        }

        Debug.Log(winner);
    }
}