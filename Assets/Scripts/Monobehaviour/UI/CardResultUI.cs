﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardResultUI : MonoBehaviour
{
    [SerializeField] List<Image> cardImages;

    public List<Image> Cards => cardImages;
}
