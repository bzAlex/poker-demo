﻿using System;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class PlayerInformation
{
    public TMPro.TextMeshProUGUI[] playerNames;
    public CardResultUI cardResult;
    public TMPro.TextMeshProUGUI handKind;
}

public class BoardUI : MonoBehaviour
{
    [SerializeField] PlayerInformation player1;
    [SerializeField] PlayerInformation player2;
    [SerializeField] PlayingCardsSpritesLibrary smallCardsUI;
    [SerializeField] Button playButton;

    [SerializeField] GameObject[] resultLabels;

    public Button PlayButton => playButton;

    public PlayerInformation Player1 { get => player1; set => player1 = value; }
    public PlayerInformation Player2 { get => player2; set => player2 = value; }

    void Start()
    {
        foreach (var r in resultLabels)
        {
            r.SetActive(false);
        }
    }

    public void SetPlayerNames(string p1, string p2)
    {
        foreach (var p in player1.playerNames)
        {
            p.text = p1;
        }
        foreach (var p in player2.playerNames)
        {
            p.text = p2;
        }
    }

    public void ShowResultingCard(Player p1, Player p2)
    {
        // List cards for each
        for (int i = 0; i < p1.handResult.resultCards.Count(); ++i)
        {
            player1.cardResult.Cards[i].sprite = smallCardsUI.GetCardSprite(p1.handResult.resultCards.ToArray()[i]);
        }
        for (int i = 0; i < p2.handResult.resultCards.Count(); ++i)
        {
            player2.cardResult.Cards[i].sprite = smallCardsUI.GetCardSprite(p2.handResult.resultCards.ToArray()[i]);
        }

        //Show hand kind
        player1.handKind.text = p1.handResult.kind.ToString();
        player2.handKind.text = p2.handResult.kind.ToString();

        // Dim cards not included

    }


    public void DetermineWinner(int result)
    {
        foreach (var r in resultLabels)
        {
            r.SetActive(false);
        }

        if (result == 0)
        {
            resultLabels[0].gameObject.SetActive(true);
        }
        else if (result == 1)
        {
            resultLabels[1].gameObject.SetActive(true);
        }
        else
            resultLabels[2].gameObject.SetActive(true);

    }
}