﻿using System.Collections;
using UnityEngine;

public class CardDistributionController : MonoBehaviour
{
    [SerializeField] SpriteRenderer[] player1Hand;
    [SerializeField] SpriteRenderer[] player2Hand;
    [SerializeField] SpriteRenderer[] communityCardsSprites;

    [SerializeField] PlayingCardsSpritesLibrary spriteLibrary;
    [SerializeField] LeanTweenType tweenType;

    public void DistributePlayerCards(Card[] player1, Card[] player2)
    {
        for (int i = 0; i < player1.Length; i++)
        {
            player1Hand[i].sprite = spriteLibrary.GetCardSprite(player1[i]);
            LeanTween.scale(player1Hand[i].gameObject, Vector3.one * 1.1f, .1f).setLoopPingPong(1).setEase(LeanTweenType.easeInSine);
            player2Hand[i].sprite = spriteLibrary.GetCardSprite(player2[i]);
            LeanTween.scale(player2Hand[i].gameObject, Vector3.one * 1.1f, .1f).setLoopPingPong(1).setEase(LeanTweenType.easeInSine);
        }
    }

    public void RevealCommunityCards(Card[] cards)
    {
        StartCoroutine(CommunityCardAnimation(cards));
    }

    IEnumerator CommunityCardAnimation(Card[] cards)
    {
        for (int i = 0; i < cards.Length; ++i)
        {
            var sprite = spriteLibrary.GetCardSprite(cards[i]);
            var index = i;
            communityCardsSprites[i].GetComponent<AudioSource>().Play();

            LeanTween.scaleX(communityCardsSprites[i].gameObject, 0, .05f)
                .setEase(LeanTweenType.easeOutSine)
                .setOnComplete
                (() =>
                {
                    communityCardsSprites[index].sprite = sprite;
                    LeanTween.scaleX(communityCardsSprites[index].gameObject, 1, .05f);
                }
            );
            yield return new WaitForSeconds(.05f);
        }
    }
}