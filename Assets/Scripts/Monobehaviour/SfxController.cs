﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SfxController : MonoBehaviour
{
    [SerializeField] AudioSource winSfx;

    public void WinSound()
    {
        winSfx.Play();
    }
}
