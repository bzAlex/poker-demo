﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Evaluates a hand
/// </summary>
public class HandEvaluator
{
    readonly static List<Card.Rank> lowAceRankSequence = new List<Card.Rank>() { Card.Rank.Ace, Card.Rank.Two, Card.Rank.Three, Card.Rank.Four, Card.Rank.Five };

    public int CompareHands(Card[] handA, Card[] handB, out HandResult resultA, out HandResult resultB)
    {
        resultA = GetHandResult(handA);
        resultB = GetHandResult(handB);

        // If different
        if (resultA.kind != resultB.kind)
            return (int)resultA.kind > (int)resultB.kind ? 1 : -1;

        // Tie-breaker
        return CompareHandsOfSameType(resultA.resultCards.ToArray(), resultB.resultCards.ToArray(), resultA.kind);
    }

    public HandResult GetHandResult(IEnumerable<Card> set)
    {
        set = set.OrderByDescending(c => c.rank);

        return ContainsRoyalFlush(set) ?? ContainsStraightFlush(set) ??
            ContainsFourOfAKind(set) ?? ContainsFullHouse(set) ?? ContainsFlush(set) ??
            ContainsStraight(set) ?? ContainsThreeOfAKind(set) ?? ContainsTwoPair(set) ??
            ContainsOnePair(set) ?? ContainsHighCard(set);
    }

    /// <summary>
    /// Arrange card in order descendingly
    /// </summary>
    IEnumerable<Card> ArrangeHand(IEnumerable<Card> cards)
    {
        cards = cards.OrderByDescending(p => p.rank);

        // Check if low ace sequence and move ace to first spot
        if (HasLowAce(cards))
        {
            var first = cards.First();
            var a = cards.ToList();
            a.Insert(5, first);
            a.RemoveAt(0);

            return a;
        }

        // For few of kinds, group by rank-> order by count-> order by rank
        return cards.GroupBy(c => c.rank).OrderByDescending(g => g.Count()).SelectMany(g => g.OrderByDescending(c => c.rank));
    }

    int CompareHandsOfSameType(Card[] pokerHand1, Card[] pokerHand2, HandType handKind)
    {
        // verify if 5 hand card
        if (!(pokerHand1.Count() == 5 && pokerHand2.Count() == 5))
            Debug.LogError("Hands are not 5 cards");

        // Arrange in set of kinds for easier comparison
        var sortedCard1 = ArrangeHand(pokerHand1).ToArray();
        var sortedCard2 = ArrangeHand(pokerHand2).ToArray();

        switch (handKind)
        {
            // Compare first card - the biggest rank with ace being low or high
            case HandType.RoyalFlush:
            case HandType.StraightFlush:
            case HandType.Straight:
                return pokerHand1[0].CompareTo(pokerHand2[0]);
        }

        //Continue comparing the hands.
        switch (handKind)
        {
            case HandType.Flush:
            case HandType.HighCard:
            case HandType.FourOfAKind:
            case HandType.FullHouse:
            case HandType.ThreeOfAKind:
            case HandType.TwoPairs:
            case HandType.OnePair:
                for (int i = 0; i < 5; ++i)
                {
                    var result = sortedCard1[i].CompareTo(sortedCard2[i]);
                    // If not the same, which is tie breaker
                    if (result != 0)
                        return result;
                }
                return 0;
        }

        // If failed to compare
        throw new Exception("Hand comparison failed.");
    }

    HandResult ContainsFlush(IEnumerable<Card> set)
    {
        // Get 5 card same suit
        var sequence = set.GroupBy(c => c.suit).Where(g => g.Count() >= 5).SelectMany(g => g.OrderByDescending(c => c.rank).Take(5));

        return sequence.Any() ? new HandResult(ArrangeHand(sequence), HandType.Flush) : null;
    }

    HandResult ContainsFourOfAKind(IEnumerable<Card> set)
    {
        var isFourOfAKind = set.GroupBy(c => c.rank).Any(g => g.Count() >= 4);

        if (isFourOfAKind)
            return new HandResult(ArrangeHand(set).Take(5), HandType.FourOfAKind);

        return null;
    }

    HandResult ContainsFullHouse(IEnumerable<Card> set)
    {
        var threeOfAKind = set.GroupBy(c => c.rank).FirstOrDefault(g => g.Count() >= 3);

        if (threeOfAKind != null)
        {
            var remainingCards = set.Except(threeOfAKind);

            var highestTwoPair = remainingCards.GroupBy(c => c.rank).
                                                Where(g => g.Count() >= 2).
                                                OrderByDescending(g => g.Key).
                                                FirstOrDefault() ?? Enumerable.Empty<Card>();

            if (highestTwoPair.Any())
                return new HandResult(threeOfAKind.Concat(highestTwoPair.Take(2)), HandType.FullHouse);
        }
        return null;
    }

    HandResult ContainsHighCard(IEnumerable<Card> set)
    {
        return new HandResult(set.OrderByDescending(x => x.rank).Take(5), HandType.HighCard);
    }

    HandResult ContainsOnePair(IEnumerable<Card> set)
    {
        if (set.GroupBy(x => x.rank).Any(x => x.Count() >= 2))
            return new HandResult(ArrangeHand(set).Take(5), HandType.OnePair);

        return null;
    }

    HandResult ContainsRoyalFlush(IEnumerable<Card> set)
    {
        var straightflush = ContainsStraightFlush(set);

        if (straightflush != null)
        {
            var isRoyal = straightflush.resultCards.Any(x => x.rank == Card.Rank.Ace) && straightflush.resultCards.Any(x => x.rank == Card.Rank.King);
            return isRoyal ? new HandResult(ArrangeHand(straightflush.resultCards), HandType.RoyalFlush) : null;
        }
        return null;
    }

    public class CompareCardEquality : IEqualityComparer<Card>
    {
        public bool Equals(Card x, Card y)
        {
            return x.rank == y.rank;
        }

        public int GetHashCode(Card obj)
        {
            return base.GetHashCode();
        }
    }


    HandResult ContainsStraight(IEnumerable<Card> set)
    {
        // Check if low ace
        if (HasLowAce(set))
            return new HandResult(ArrangeHand(set.Where(x => lowAceRankSequence.Contains(x.rank)).GroupBy(x => x.rank).Select(x => x.First())), HandType.Straight);

        // If not low ace, check best possible straight
        Card.Rank? previous = null;
        int consecutiveIndex = 0;

        // else test if set contains consecutive, 
        set = set.OrderByDescending(c => c.rank);
        // todo remove duplicates 
        var distinctSets = set.Distinct(new CompareCardEquality());

        //todo  Check if hand is 5, if not, return null
        if (distinctSets.Count() < 5)
            return null;

        for (int i = 0; i < distinctSets.Count(); i++)
        {
            // compare last
            if (previous.HasValue)
            {
                // If not in sequence, restart our consecutive index at i
                if (previous != distinctSets.ElementAt(i).rank + 1)
                {
                    consecutiveIndex = i;
                }
                // If gotten 5 consecutives, return
                if (i - consecutiveIndex == 4)
                {
                    return new HandResult(ArrangeHand(distinctSets.Skip(consecutiveIndex).Take(5)), HandType.Straight);
                }
            }
            previous = distinctSets.ElementAt(i).rank;
        }

        return null;
    }

    HandResult ContainsStraightFlush(IEnumerable<Card> set)
    {
        // get all same suit cards
        // Return suit group with more than 5
        var sameSuitCards = set.GroupBy(c => c.suit).FirstOrDefault(g => g.Count() >= 5);

        if (sameSuitCards == null)
            return null;

        var straight = ContainsStraight(sameSuitCards);
        return straight != null ? new HandResult(ArrangeHand(straight.resultCards), HandType.StraightFlush) : null;
    }

    HandResult ContainsThreeOfAKind(IEnumerable<Card> set)
    {
        bool isThreeOfAKind = set.GroupBy(c => c.rank).Any(g => g.Count() >= 3);

        if (isThreeOfAKind)
            return new HandResult(ArrangeHand(set).Take(5), HandType.ThreeOfAKind);

        return null;
    }

    HandResult ContainsTwoPair(IEnumerable<Card> set)
    {
        if (set.GroupBy(c => c.rank).Where(g => g.Count() == 2).Count() >= 2)
            return new HandResult(ArrangeHand(set).Take(5), HandType.TwoPairs);

        return null;
    }

    bool HasLowAce(IEnumerable<Card> cards)
    {
        return lowAceRankSequence.All(r => cards.Any(c => c.rank == r));
    }
}