﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Tester : MonoBehaviour
{
    HandEvaluator handEvaluator = new HandEvaluator();

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            //    var flush = new List<Card>()
            //{
            //    new Card(Card.Suit.Clubs,Card.Rank.Five),
            //    new Card(Card.Suit.Clubs,Card.Rank.Four),
            //    new Card(Card.Suit.Clubs,Card.Rank.Three),
            //    new Card(Card.Suit.Clubs,Card.Rank.Two),
            //    new Card(Card.Suit.Clubs,Card.Rank.Ace),
            //    new Card(Card.Suit.Clubs,Card.Rank.Ten),
            //    new Card(Card.Suit.Clubs,Card.Rank.Nine),

            //};
            var royal = new List<Card>()
            {
                new Card(Card.Suit.Clubs,Card.Rank.Ace),
                new Card(Card.Suit.Clubs,Card.Rank.King),
                new Card(Card.Suit.Clubs,Card.Rank.Queen),
                new Card(Card.Suit.Clubs,Card.Rank.Jack),
                new Card(Card.Suit.Clubs,Card.Rank.Ten),
                new Card(Card.Suit.Clubs,Card.Rank.Nine),
                new Card(Card.Suit.Clubs,Card.Rank.Eight),
            };

            var fullHouse = new List<Card>()
        {
            new Card(Card.Suit.Hearts,Card.Rank.Two),
            new Card(Card.Suit.Spades,Card.Rank.Nine),
            new Card(Card.Suit.Hearts,Card.Rank.Eight),
            new Card(Card.Suit.Spades,Card.Rank.Six),
            new Card(Card.Suit.Clubs,Card.Rank.Six),
            new Card(Card.Suit.Clubs,Card.Rank.Five    ),
            new Card(Card.Suit.Hearts,Card.Rank.Seven),
        };
            var fullHouse2 = new List<Card>()
        {
            new Card(Card.Suit.Clubs,Card.Rank.Ace),
            new Card(Card.Suit.Clubs,Card.Rank.Two),
            new Card(Card.Suit.Clubs,Card.Rank.Three),
            new Card(Card.Suit.Clubs,Card.Rank.Five),
            new Card(Card.Suit.Clubs,Card.Rank.Four),
            new Card(Card.Suit.Clubs,Card.Rank.Nine),
            new Card(Card.Suit.Clubs,Card.Rank.Eight),
        };

            var A = new Player(0, "a");
            var B = new Player(1, "b");
            Debug.Log(handEvaluator.CompareHands(royal.ToArray(), fullHouse.ToArray(), out A.handResult, out B.handResult));
            Print(A);
            Print(B);
        }

        void Print(Player player)
        {
            var s = "";
            foreach (var r in player.handResult.resultCards)
            {
                s = String.Concat(s, (r.ToString() + " "));
            }
            Debug.Log(player.name + " = " + player.handResult.kind + "=>" + s);
        }

        void DisplayCard(IEnumerable<Card> cards)
        {
            var s = "";
            foreach (var c in cards)
            {
                s = s + " " + c.ToString();
            }
            Debug.Log(s);
        }
    }
}