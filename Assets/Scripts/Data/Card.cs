﻿using System;

public struct Card : IComparable<Card>
{
    public enum Suit { Hearts, Spades, Diamonds, Clubs }

    public enum Rank { Two = 2, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King, Ace }

    public Suit suit { get; }
    public Rank rank { get; }

    public string name { get; }

    public Card(Suit suit, Rank rank, string name)
    {
        this.suit = suit;
        this.rank = rank;
        this.name = name;
    }

    public Card(Suit suit, Rank rank) : this(suit, rank, suit + "-" + rank)
    {
    }

    public override string ToString()
    {
        return name;
    }
    public override bool Equals(object obj)
    {
        return base.Equals(obj);
    }


    /// <summary>
    /// Compares card's rank
    /// </summary>
    public int CompareTo(Card other)
    {
        //Aces are always treated as high aces in this function.
        //Low aces are never passed to this function.

        var val1 = (int)this.rank;
        var val2 = (int)other.rank;
        return val1 > val2 ? 1 : val1 == val2 ? 0 : -1;
    }
}