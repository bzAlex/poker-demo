﻿using System;
using System.Collections.Generic;
using System.Linq;
using static Card;

public static class DeckCreator
{
    public static Queue<Card> CreateDeck()
    {
        var cards = new Queue<Card>();

        foreach (var suit in Enum.GetValues(typeof(Suit)))
        {
            foreach (var rank in Enum.GetValues(typeof(Rank)))
                cards.Enqueue(new Card((Suit)suit, (Rank)rank, GetShortName((Rank)rank, (Suit)suit)));
        }

        return Shuffle(cards);
    }

    static Queue<Card> Shuffle(Queue<Card> cards)
    {
        //Shuffle the existing cards using Fisher-Yates Modern
        List<Card> transformedCards = cards.ToList();
        Random r = new Random(DateTime.Now.Millisecond);
        for (int n = transformedCards.Count - 1; n > 0; --n)
        {
            //Step 2: Randomly pick a card which has not been shuffled
            int k = r.Next(n + 1);

            //Step 3: Swap the selected item with the last "unselected" card in the collection
            Card temp = transformedCards[n];
            transformedCards[n] = transformedCards[k];
            transformedCards[k] = temp;
        }

        var shuffledCards = new Queue<Card>();
        foreach (var card in transformedCards)
        {
            shuffledCards.Enqueue(card);
        }

        return shuffledCards;
    }

    static string GetShortName(Rank rank, Suit suit)
    {
        var shortSuit = new string[] { "H", "S", "D", "C" };
        var shortRank = new string[] { "0", "0", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A" };

        return shortRank[(int)rank].ToString() + shortSuit[(int)suit].ToString();
    }

    public static IEnumerable<T> DequeueChunk<T>(this Queue<T> queue, int chunkSize)
    {
        for (int i = 0; i < chunkSize && queue.Count > 0; i++)
        {
            yield return queue.Dequeue();
        }
    }
}