﻿using System.Collections.Generic;

public class Player
{
    public int playerID;
    public string name;
    public List<Card> cardsInHand = new List<Card>();

    public HandResult handResult;

    public Player(int playerID, string name)
    {
        this.playerID = playerID;
        this.name = name;
    }

    /// <summary>
    /// Gets the player's 2 card-set along with 5 community cards
    /// </summary>
    public IEnumerable<Card> GetPlayerSet()
    {
        return cardsInHand;
    }
}