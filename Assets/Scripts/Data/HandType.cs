﻿public enum HandType
{
    HighCard,
    OnePair,
    TwoPairs,
    ThreeOfAKind,
    Straight,
    FullHouse,
    Flush,
    FourOfAKind,
    StraightFlush,
    RoyalFlush
}