﻿using System;
using System.Collections.Generic;

public class HandResult : IComparable<HandResult>
{
    public HandResult(IEnumerable<Card> resultCards, HandType result)
    {
        this.kind = result;
        this.resultCards = resultCards;
    }

    public HandType kind { get; set; }

    /// <summary>
    /// A 5 card set
    /// </summary>
    public IEnumerable<Card> resultCards { get; set; }

    public int CompareTo(HandResult other)
    {
        if (other == null)
            return 1;

        // If of same hand kind, compare by rank total
        var result = kind.CompareTo(other.kind);

        return result;
    }
}